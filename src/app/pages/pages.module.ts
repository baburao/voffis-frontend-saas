import { NgModule} from '@angular/core';
import { CountriesComponent } from './Configurations/countries/countries.component';
import { CurrencyComponent } from './Configurations/currency/currency.component';
import { DepartmentsComponent } from './Configurations/departments/departments.component';
import { ElementsComponent } from './Configurations/elements/elements.component';
import { PackagesComponent } from './Configurations/packages/packages.component';
import { RolePermissionsComponent } from './Security/role-permissions/role-permissions.component';
import { UserPermissionsComponent } from './Security/user-permissions/user-permissions.component';
import { EmployeeComponent } from './employee/employee.component';
import { RouterModule, Routes } from '@angular/router';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseHighlightModule } from '@fuse/components';
import { MatRippleModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FuseConfirmDialogModule, FuseSidebarModule } from '@fuse/components';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTabsModule } from '@angular/material/tabs';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatChipsModule } from '@angular/material/chips';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSliderModule } from '@angular/material/slider';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatStepperModule } from '@angular/material/stepper';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';

const routes: Routes = [
    {
        path     : 'configurations/countries',
        component: CountriesComponent
    },
    {
        path     : 'configurations/currency',
        component: CurrencyComponent
    },
    {
        path     : 'configurations/departments',
        component: DepartmentsComponent
    },
    {
        path     : 'configurations/elements',
        component: ElementsComponent
    },
    {
        path     : 'configurations/packages',
        component: PackagesComponent
    },
    {
        path     : 'security/role',
        component: RolePermissionsComponent
    },
    {
        path     : 'security/user',
        component: UserPermissionsComponent
    },
    {
        path     : 'employee',
        component: EmployeeComponent
    },
];

@NgModule({
    declarations: [CountriesComponent, CurrencyComponent, DepartmentsComponent, ElementsComponent, PackagesComponent, RolePermissionsComponent, UserPermissionsComponent, EmployeeComponent],
    imports     : [
        RouterModule.forChild(routes),
        MatIconModule,
        MatTabsModule,
        FuseSharedModule,
        FuseHighlightModule,
        MatButtonModule,
        MatCheckboxModule,
        MatRippleModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatInputModule,
        MatMenuModule,
        MatTableModule,
        MatToolbarModule,
        FuseConfirmDialogModule,
        FuseSidebarModule,
        MatListModule,
        MatSidenavModule,
        MatSlideToggleModule,
        FormsModule,
        MatExpansionModule,
        MatSelectModule,
        MatCardModule,
        MatProgressBarModule,
        MatChipsModule,
        MatGridListModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatRadioModule,
        MatSliderModule,
        MatSnackBarModule,
        MatSortModule,
        MatTooltipModule,
        MatStepperModule,
        ReactiveFormsModule,
        
    ]
})
export class PagesModule
{

}
