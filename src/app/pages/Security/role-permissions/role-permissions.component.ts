import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'app-role-permissions',
  templateUrl: './role-permissions.component.html',
  styleUrls: ['./role-permissions.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations   : fuseAnimations
})
export class RolePermissionsComponent implements OnInit {

  displayedColumns = ['Screen', 'list1', 'list2', 'list3'];
  dataSource = [
    {Screen: 'Designation',  list1: true, list2: true, list3: true },
    {Screen: 'Department',  list1: true, list2: true, list3: true },
    {Screen: 'Designation',  list1: true, list2: true, list3: true },
    {Screen: 'Designation',  list1: true, list2: true, list3: true },
    {Screen: 'Designation',  list1: true, list2: true, list3: true },
    {Screen: 'Designation',  list1: true, list2: true, list3: true },
    {Screen: 'Designation',  list1: true, list2: true, list3: true },
  ];
  constructor() { }
  

  ngOnInit() {
    this.getactiveroles();
  }


  Contact(role): void {
    console.log(role);
  }
  onSelectedChange(id){
    console.log(id)
  }

  //GETTING ACTIVE ROLES
  getactiveroles(){
    
  }

}
