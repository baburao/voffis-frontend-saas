import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id       : 'applications',
        title    : '',
        translate: 'NAV.APPLICATIONS',
        type     : 'group',
        icon     : 'apps',
        children : [
            {
                id       : 'configurations',
                title    : 'Configurations',
                type     : 'collapsable',
                icon     : 'settings',
                children : [
                    {
                        id   : 'countries',
                        title: 'Countries',
                        type : 'item',
                        url  : '/configurations/countries'
                    },
                    {
                        id   : 'currency',
                        title: 'Currency',
                        type : 'item',
                        url  : '/configurations/currency'
                    },
                    {
                        id   : 'departments',
                        title: 'Departments',
                        type : 'item',
                        url  : '/configurations/departments'
                    },
                    {
                        id   : 'elements',
                        title: 'Elements',
                        type : 'item',
                        url  : '/configurations/elements'
                    },
                    {
                        id   : 'packages',
                        title: 'Packages',
                        type : 'item',
                        url  : '/configurations/packages'
                    }
                ]
            },
            {
                id       : 'security',
                title    : 'Security',
                type     : 'collapsable',
                icon     : 'security',
                children : [
                    {
                        id   : 'role',
                        title: 'Role Permissions',
                        type : 'item',
                        url  : '/security/role'
                    },
                    {
                        id   : 'user',
                        title: 'User Permissions',
                        type : 'item',
                        url  : '/security/user'
                    }
                ]
            },
            {
                id   : 'employee',
                title: 'Employee',
                type : 'item',
                icon : 'person',
                url  : '/employee'
            },
        ]
    }
];
